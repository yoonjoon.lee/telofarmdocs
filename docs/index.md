# TELOFARM Docs

Documentations for TeloFarm

### Table of Contents

1. [Introduction](intro.md)

2. [Authentification](authentification.md)

3. [Authorization](authorization.md)

4. [Device Managemant](devicemanagement.md)
