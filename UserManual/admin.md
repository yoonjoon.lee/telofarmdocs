## 4. 관리(Admin) 메뉴

System 관리를 위한 기능을 제공한다. Web 화면 상단 오른쪽 설정 버늩을 click하여 활성화할 수 있다. click한 후 FIg. 4.0 화면이 나타닌다.

![admin_00](img/admin_00.png "Administration 시작 화면")
<center>Fig. 4.0 Administration 시작 화면</center>
<br><br>

### 4.1. System 관제 (Monitoring)

> (2021-05-05 현재) 아래 Fig. 4.11 상태이며, 아직 기능은 구현 중인 것 같음.

![admin_20](img/admin_20.png "System Monitoring 화면")
<center>Fig. 4.10 System Monitoring 화면</center>
<br><br>

#### 4.1.1. 시스템 자원 상태 표시

#### 4.1.2. 소프트웨어 상태 표시

#### 4.1.3. 소프트웨어 제어 (실행/정지)

### 4.2. 사이트 관리 (목록 뷰)

사이트 검색, 등록, 삭제 및 즐겨 찾는 (Section 1.4 사이트 카드 뷰 참조) site를 지정하는 기능을 제공한다.

#### 4.2.1 사이트 목록 뷰

Fig. 4.0 화면 왼쪽 Side bar에서 "Sites"를 선택하여 clcik하면 Site 목록이 Fig. 4.21과 같이 보여 준다. 이 화면에서 사이트를 등록, 삭제 및 즐겨 찾는 site를 지정할 수 있다.

![admin_21](img/admin_21.png "Sites 목록 화면")
<center>Fig. 4.21 Sites 목록 화면</center>
<br><br>

#### 4.2.2. 사이트 상세 정보

사이트 목록에 한 사이트에 해당하는 row를 클릭하면 그 사이트에 대한 상세 정보를 popup 창으로 볼 수 있다 (Fig. 4.22). 사이트에 대한 정보(SITE INFO)는 Site ID, Site Name, Type, Project, Location (Latitude와 Longitude), Zipcode, Address1, Address2, Owners, Users, Last Modified time, Created Time와 Description이다.

> 각 함목에 대한 설명이 필요함.

![admin_22](img/admin_22.png "사이트 상세 정보 화면")
<center>Fig. 4.22 사이트 상세 정보 화면</center>
<br><br>

또한 Site 이력 정보(SITE HISTORY)를 제공한다. Name(이력을 추가한 User Name(?)), Code(해당 operation code), Desc(Code에 대한 설명)과 Date(operation 수행 시각)으로 한 이력을 구성한다.

우측 상단의 "CLOSE" button을 click하여 사이트 상세 정보 보기를 창을 닫을 수 있다.

#### 4.2.3. 사이트 추가/수정

##### 4.2.3.1. 사이트 추가

Fig. 4.21 화면 상단의 "ADD" button을 click하면 나타나는 popup 창 (Fig. 4.23)에서 사이트 추가를 할 수 있다.

![admin_23](img/admin_23.png "사이트 추가 화면")
<center>Fig. 4.23 사이트 추가 화면</center>
<br><br>

이 윈도우에서 "INFO" button을 click하여 "INFO" 화면에서(default 화면) Site ID, Site Name, Type, Project, Zipcode, Address1, Address2과 Description을 입력한 다음 "ADD" button을 click하여 사이트를 추가한다.

사이트를 추가한 다음 "User" button을 click하여 "USER" 화면(Fig. 4.24)으로 이동한다. Fig. 4.24 화면은 시스템에 등록된 모든 사용자(?) 목록을 보여준다. 이때 추가하고자 하는 모든 사용자의 왼쪽 check box를 click하여 사용자를 선택한 다음 하단의 "ADD" button을 click하여 사이트의 사용자로 등록할 수 있다.

![admin_24](img/admin_24.png "사이트 사용자 추가 화면")
<center>Fig. 4.24 사이트 사용자 추가 화면</center>
<br><br>

아직 등록되지 않은 새로운 사용자를 등록하려면 하단 오른쪽 "NEW USER" button을 click하여 "ADD USER" 화면(Fig. 4.25)으로 이동한다. 화면에서 Name, Country code, Phone number, E-mail, Organization과 Description을 입력한 다음, 하단의 오른쪽 "ADD" button을 click하여 사이트의 새로운 사용자를 등록할 수 있다. 또한 하단 왼쪽의 "CANCEL" button을 clcik하면 새로운 사용자 추가를 취소할 수 있다.

![admin_25](img/admin_25.png "사이트 새로운 사용자 추가 화면")
<center>Fig. 4.25 사이트 새로운 사용자 추가 화면</center>
<br><br>

##### 4.2.3.2. 사이트 수정

Fig. 4.21 사이트 목록 화면에서 각 사이트 행 오른쪽 끝의 연필 모양 icon을 click하면 나타나는 popup 창 (Fig. 4.26)에서 사이트 정보를 수정할 수 있다.

![admin_26](img/admin_26.png "사이트 정보 수정 화면")
<center>Fig. 4.26 사이트 정보 수정 화면</center>
<br><br>

위의 사이트 정보 수정 화면은 Fig 4.23 사이트 추가 화면과 흡사하다. 이 윈도우에서 상단 "INFO" button을 click하여 "INFO" 화면에서(default 화면) 수정하고자 하는 항목의 값을 입력한 다음 하단 오른쪽 "UPDATE" button을 click하여 사이트 정보를 수정할 수 있다.

사이트 정보를 수정한 다음 "User" button을 click하여 "USER" 화면(Fig. 4.27)으로 이동한다. Fig. 4.27 화면은 시이트에 등록된 모든 사용자 목록을 보여준다. 이때 수정하고자 하는 사용자의 왼쪽 check box를 click하여 사용자를 선택한 다음 하단의 "UPDATE" button을 click하면 사이트의 사용자 정보를 수정할 수 있다 (Fig. 4.28).

![admin_27](img/admin_27.png "사이트 사용자 수정 화면")
<center>Fig. 4.27 사이트 사용자 수정 화면</center>
<br><br>

![admin_28](img/admin_28.png "사이트 사용자 정보 수정 화면")
<center>Fig. 4.28 사이트 사용자 정보 수정 화면</center>
<br><br>

> 위의 Fig. 4.28의 의도를 이해하지 못함.

아직 등록되지 않은 새로운 사용자를 등록하려면 Fig. 4.27에서 하단의 "NEW USER" button을 click하여 "ADD USER" 화면(Fig. 4.25)으로 이동한다. 화면에서 Name, Country code, Phone number, E-mail, Organization과 Description을 입력한 다음, 하단의 오른쪽 "ADD" button을 click하여 사이트의 새로운 사용자를 등록할 수 있다. 또한 하단 왼쪽의 "CANCEL" button을 clcik하면 새로운 사용자 추가를 취소할 수 있다.

#### 4.2.4. 사이트 삭제

Fig. 4.21 사이트 목록 뷰 화면에 사이트 row 왼쪽 chcek box를 click한 다음 (Fig. 4.29) 화면 상단의 "DEL" button을 clcik하면 삭제 확인 popup window(Fig 4.30)가 나타나고 하단 오른쪽 "DELETE" button을 click하여 사이트를 삭제 할 수 있고, 하단 왼쪽 "CANCEL" button을 click하여 삭제를 취소할 수 있다.

![admin_29](img/admin_29.png "삭제할 사이트 선택 화면")
<center>Fig. 4.29 삭제할 사이트 선택 화면</center>
<br><br>

![admin_30](img/admin_30.png "사이트 삭제 확인 화면")
<center>Fig. 4.30 사이트 삭제 확인 화면</center>
<br><br>

#### 4.1.5. 사이트 정보 이관

> (2021-05-05 현재) 사이트 정보 이관에 대한 기능이 정의되어 있지 않은 것같음

### 4.3. Subjects 유형 관리

Sensor 설치 대상 (subject)에 대한 정보를 관리하는 기능을 제공한다. Fig. 4.0 Administration 시작 화면 좌측 side bar menu에서 "Subjects" button을 click하여 시작할 수 있다. subject는 Subject Type (tree, soil 등)과 이들을 세분화한 subject family (maple, ocher, pinus_densiflora 등)으로 구성된다. 전체적인 목록 뷰는 Fig. 4.31과 같다.

![admin_31](img/admin_31.png "Subject family 목록 화면")
<center>Fig. 4.31 Subject family 목록 화면</center>
<br><br>

#### 4.3.1. Subjects family 목록 뷰

Subjects family 전체 목록 리스트(Fig. 4.31)는 위의 설명을 참조하시오.

##### 4.3.1.1. Subjects 목록 필터링

검색하고자 하는 subject type을 상단 "Subject Type:" drop down 메뉴를 clck하면 현재 등록된 모든 subject type 리스트를 보여주면 이들 증 하나를 click함으로써 필터링을 수행한다.

![admin_32](img/admin_32.png "Subject 목록 필터링 화면")
<center>Fig. 4.32 Subject 목록 필터링 화면</center>
<br><br>

##### 4.3.1.2. Subjects 목록 검색

> (2021-05-05 현재) 어떤 것을 의미하는 지 애매 모호함.

Fig. 4.31 화면 상단 오른쪽 검색창에 검색하고자 하는 subject type, subject family ID 또는 subject family name을 입력하면 검색 결과를 볼 수 있다.

![admin_33](img/admin_33.png "Subject 목록 검색 화면")
<center>Fig. 4.33 Subject 목록 검색 화면</center>
<br><br>

#### 4.3.2. Subjects type/family 추가/편집/삭제

##### 4.3.2.1. Subjects type 추가

> (2021-05-05 현재) subject type 추가는 어땋게 하는가?

##### 4.3.2.2. Subjects family 추가

Fig. 4.31 Subject family 목록 화면 상단 "ADD" button을 click하면 Fig. 4.34와 같은 subject family 추가흫 위한 popup window가 나타난다.

![admin_34](img/admin_34.png "Subject family 추가 화면")
<center>Fig. 4.34 Subject family 추가 화면</center>
<br><br>

Fig. 4.34 window에서 Subject Type을 drop down 메뉴에서 선택하고, Subject Family ID와 Subject Family Name을 입력한 다음, 하단 오른쪽 "ADD" button을 cllick하여 subject family를 추가할 수 있으며, 하단 왼쪽 "CANCEL" button을 click하며 추가 작업을 취소할 수 있다.

##### 4.3.2.3. Subjects type 삭제

> (2021-05-05 현재) subject type 삭제는 어땋게 하는가?

##### 4.3.2.4. Subjects family 삭제

Fig. 4.31 Subject family 목록 화면에서 삭제할 subject family ID row의 왼쪽 check box를 click하여 삭제할 subject family를 표시한다 (Fig. 4.36). 다음 상단 "DEL" button을 click한다.

![admin_36](img/admin_36.png "Subject family 삭제 화면")
<center>Fig. 4.36 Subject family 삭제 화면</center>
<br><br>

상단 "DEL" button을 click하면 Fig. 4.37와 같은 삭제 확인 popup window가 나타난다.

![admin_37](img/admin_37.png "Subject family 삭제 확인 화면")
<center>Fig. 4.37 Subject family 삭제 확인 화면</center>
<br><br>

Fig. 4.37 Subject family 삭제 확인 화면에서 하단 오른쪽 "DELETE" button을 cllick하여 subject family를 삭제할 수 있으며, 하단 왼쪽 "CANCEL" button을 click하며 이를 취소할 수 있다.

##### 4.3.2.5. Subjects type 편집

> (2021-05-05 현재) subject type 편집은 어땋게 하는가?

##### 4.3.2.6. Subjects family 편집

Fig. 4.31 Subject family 목록 화면에서 subject family row의 오른쪽 연필 icon을 click하면 subject family 편집 popup window (Fig. 4.39)가 나타난더.

![admin_39](img/admin_39.png "Subject family 편집 화면")
<center>Fig. 4.39 Subject family 편집 화면</center>
<br><br>

popup window에서 Subject Family Name만을 변경할 수 있다. 이를 입력한 다음 하단 오른쪽 "UPDATE" button을 click하여 변경할 수 있으며, 하단 왼쪽의 "CANCEL" button을 click하여 편집을 취소할 수 있다.

### 4.4. Product 관리

이 기능은 telofarm에서 생산하여 고객에게 납품하는 모든 product에 대한 관리 기능을 제공한다. 모든 device는 product로 정의 되어 있어야 한다. Fig. 4.0 Administration 시작 화면 좌측 side bar menu에서 "Products" button을 click하여 시작할 수 있다. Product는 Type는 Gateway, Modiler과 Sensor)으로 나누어 지며, ID, Name, 적용되는 Algorithm (Section 4.5 참조)과 마지막 변경일을 갖는다.

#### 4.4.1. Product 목록 뷰

Product 관리를 위한 화면 즉 Fig. 4.0 Administration 시작 화면 좌측 side bar menu에서 "Products" button을 click한 다음 보이는 화면으로 Fig. 4.41과 같다.

![admin_41](img/admin_41.png "Product 목록 뷰 화면")
<center>Fig. 4.41 Product 목록 뷰 화면</center>
<br><br>

##### 4.4.1.1. Product 목록 필터링

> Product 목록을 필터링 한다면 위의 Section 4.3.1.1 Subjects 목록 필터링과 같이 product type에 대한 drop down menu가 필요할 것 같다.

##### 4.4.1.2. Product 목록 검색

Product 목록 검색은 위의 Section 4.3.1.2 Subjects 목록 검색과 같이 Fig. 4.41 화면 상단 오른쪽 검색창에 검색하고자 하는 ID, Name, Type, Algorithm 또는 LastModified 값을 입력하면 검색 결과를 볼 수 있다.

![admin_43](img/admin_43.png "Product 목록 검색")
<center>Fig. 4.43 Product 목록 검색 화면</center>
<br><br>

#### 4.4.2. Product 추가/삭제/편집

Product에 대한 추가, 삭제 및 편집 기능을 제공한다. product에 속한 한 device라도 생산이 되어 사용되었다면 이력 관리를 위하여 product를 삭제하지 않는 것을 추천한다.

##### 4.4.2.1. Product 추가

Product 추가는 Fig. 4.41 Product 목록 뷰 화면 상단 "ADD" button을 click하면 Fig. 4.44와 같은 popup window가 나타난다.

![admin_44](img/admin_44.png "Product 추가 화면")
<center>Fig. 4.44 Product 추가 화면</center>
<br><br>

추가하고자 하는 product의 Type, ID, Name과 Description을 입력하고 추가할 product에 연결될 수 있는 product를 "Childable(optional)" 화면에서 보여주는 product 목록에서 선택(오른쪽 check box를 click)한 다음 하단 오른쪽 "ADD"를 click하면 새로운 product를 등록할 수 있다. Product를 선택할 때 Childable 화면에서 product 필터링 기능을 사용할 수 있도 있다. 또한 입력중에 취소하려면 하단 왼쪽 "CANCEL" button을 click하여 취소할 수 있다.

##### 4.4.2.2. Product 삭제

이미 등록한 product를 삭제할 수 있다. 이는 이미 위에서 언급하였지만 이에 속하는 device가 하나라도 등록되어 사용된 적이 있다면 삭제 하지 않는 것이 바람직하다. 삭제를 위하여는 product 왼쪽 check box를 click하여 check한다 (Fig. 4.45 (a)). 다음 화면 상단 "DEL" button을 click하면 Fig. 4.45 (b) Product 삭제 확인 화면이 pup window에 나타난다. 이때 화면 하단의 "DELETE" 또는 "CANCEL" button을 click하며 삭제 또는 취소할 수 있다.

> 이미 어떤 device를 갖고 있는 product를 삭제하려고 한다면 오류 메시지를 생성하는 것이 바람직하지 않을까요?

![admin_45_a](img/admin_45_a.png "Product 삭제 화면")
<center>Fig. 4.45 (a) Product 삭제 화면</center>
<br><br>

![admin_45_b](img/admin_45_b.png "Product 삭제 확인 화면")
<center>Fig. 4.45 (b) Product 삭제 확인 화면</center>
<br><br>

##### 4.4.2.3. Product 편집

Product에 대한 정보의 변경 기능을 제공함다. Product 맨 오른쪽 연필 모양의 icon을 click하여 해당 product에 대한 정보를 편집할 수 있다. click하면 Fig. 4.46과 같은 화면이 popup window로 나타나며, Name과 Description을 변경할 수 있다. 데이터를 변경한 다음 화면 하단의 "CANCEL" 또는 "UPDATE" button을 click하면 변경을 취소하거나 수행할 수 있다.

![admin_46](img/admin_46.png "Product 편집 화면")
<center>Fig. 4.46 Product 편집 화면</center>
<br><br>

### 4.5. Algorithm 관리

Module type product에 적용되는 algorithm에 대한 관리 기능을 제공한다. 따라서 algorithm에 대한 정보와 이를 적용하는 product에 대한 정보를 관리한다.

#### 4.5.1. Algorithm 목록 뷰

 Algorithm 관리를 위한 화면 즉 Fig. 4.0 Administration 시작 화면 좌측 side bar menu에서 "Algorithms" button을 click한 다음 보이는 화면으로 Fig. 4.51과 같다.

 Algorithm 관리를 위한 화면 즉 Fig. 4.0 Administration 시작 화면 좌측 side bar menu에서 "Algorithm" button을 click한 다음 보이는 화면으로 Fig. 4.51과 같다.

![admin_51](img/admin_51.png "Algorithm 목록 뷰 화면")
<center>Fig. 4.51 Algorithm 목록 뷰 화면</center>
<br><br>

##### 4.5.1.1. Algorithm 목록 필터링

> Algorithm 목록을 필터링 한다면 위의 Section 4.3.1.1 Subjects 목록 필터링과 같이 필터링을 위한 column이 정의되어 있어야 할 것이다.

##### 4.4.1.2. Algorithm 목록 검색

Algorithm 목록 검색은 위의 Section 4.3.1.2 Subjects 목록 검색과 같이 Fig. 4.51 화면 상단 오른쪽 검색창에 검색하고자 하는 Algorithm ID, Product ID, LasteModified Time 또는 LastModified by 값을 입력하면 검색 결과를 볼 수 있다.

![admin_53](img/admin_53.png "Algorithm 목록 검색")
<center>Fig. 4.53 Algorithm 목록 검색 화면</center>
<br><br>

#### 4.5.2. Algorithm 추가/삭제/편집

Algorithmt에 대한 추가, 삭제 및 편집 기능을 제공한다. Algorithm이 한번이라도 적용되었다면 이력 관리를 위하여 Algorithm를 삭제하지 않는 것을 추천한다.

##### 4.5.2.1. Algorithm 추가

Algorithm 추가는 Fig. 4.51 Algorithm 목록 뷰 화면 상단 "ADD" button을 click하면 Fig. 4.54와 같은 popup window가 나타난다.

![admin_54](img/admin_54.png "Algorithm 추가 화면")
<center>Fig. 4.54 Algorithm 추가 화면</center>
<br><br>

추가하고자 하는 algorithm의 ID, drop down 메뉴를 이용하여 적용되는 Module ID과 적용되는 데이터 주기 "Last N hours", Code, code에서 사용하는 constants와 Test 결과를 입력하거나 추가한 다음 하단 오른쪽 "ADD"를 click하면 새로운 Algorithmd을 등록할 수 있다. 또한 입력중에 취소하려면 하단 왼쪽 "CANCEL" button을 click하여 취소할 수 있다.

> "?" 표시가 있으나 작동하지 않음

##### 4.5.2.2. Algorithm 삭제

이미 등록한 algorithm을 삭제할 수 있다. 이는 이미 위에서 언급하였지만 이에 속하는 algorithm이 한번이라도 등록되어 사용된 적이 있다면 삭제 하지 않는 것이 바람직하다. 삭제를 위하여는 algorithm 왼쪽 check box를 click하여 check한다 (Fig. 4.55 (a)). 다음 화면 상단 "DEL" button을 click하면 Fig. 4.55 (b) algorithm 삭제 확인 화면이 pup window에 나타난다. 이때 화면 하단의 "DELETE" 또는 "CANCEL" button을 click하며 삭제 또는 취소할 수 있다.

> 이미 적용하였던 algorithm을 삭제하려고 한다면 오류 메시지를 생성하는 것이 바람직하지 않을까요?

![admin_55_a](img/admin_55_a.png "Algorithm 삭제 화면")
<center>Fig. 4.55 (a) Algorithm 삭제 화면</center>
<br><br>

![admin_55_b](img/admin_55_b.png "Algorithm 삭제 확인 화면")
<center>Fig. 4.55 (b) Algorithm 삭제 확인 화면</center>
<br><br>

##### 4.5.2.3. Algorithm 편집

> Algorithm에 대한 정보의 변경 기능을 제공함다.
> Algorithm 맨 오른쪽 연필 모양의 icon을 click하여 해당 product에 대한 정보를 편집할 수 있다. (2021-05-19 현재 자동하지 않음)

### 4.6 Constants FIle

Module type product에서 사용되는 Calibration Values를 import하거나 export하는 기능을 제공한다.

Fig. 4.0 Administration 시작 화면 좌측 side bar menu에서 "Constants File" button을 click하면 Fig. 4.61과 같이 적용하고 있는 Calibration Values을 보여준다.

![admin_61](img/admin_61.png "Calibration Values 화면")
<center>Fig. 4.61 Calibration Values 화면</center>
<br><br>

### 4.6.1. Constants FIle Import

Calibration Values를 새로이 import 하려면 Fig. 4.61 Calibration Values 화면 상단의 "IMPORT" button을 click하면 Fig. 4.62와 같은 popup 화면이 나타나고 import하려는 파일을 지정한다.

![admin_62](img/admin_62.png "Constants FIle Import 화면")
<center>Fig. 4.62 Constants FIle Import 화면</center>
<br><br>

### 4.6.2. Constants FIle Export

Calibration Values를 파일로 export 하려면 Fig. 4.61 Calibration Values 화면 상단의 "EXPORT" button을 click하면 Fig. 4.63와 같이 지정된 download 폴더에 저장된다.

![admin_63](img/admin_63.png "Constants FIle Export 화면")
<center>Fig. 4.63 Constants FIle Export 화면</center>
<br><br>

> "SAVE" button이 나와 있으나 어떤 의미인지 작동하지 않음 (?)

### 4.7. Users(사용자) 관리

시스템 사용자 등록, 사용자 정보 변경 및 사용자 삭제 기능을 지원한다. Fig. 4.0 Administration 시작 화면 좌측 side bar menu에서 "Users" button을 click하여 시작할 수 있다. 사용자는 그 권한에 따라 "Unapproved", "Site User", "Site Manager", "Viewer", "Superuser", "Site Owner"로 나누어 진다.

> 각 권한에 따라 사용 권한 기술이 필요

#### 4.7.1. User 목록 뷰

Product 관리를 위한 화면 즉 Fig. 4.0 Administration 시작 화면 좌측 side bar menu에서 "Products" button을 click한 다음 보이는 화면으로 Fig. 4.71과 같다.

![admin_71](img/admin_71.png "User 목록 뷰 화면")
<center>Fig. 4.71 User 목록 뷰 화면</center>
<br><br>

##### 4.7.1.1. User 목록 필터링

> User 목록을 필터링 한다면 "Authority" 또는 "Organization"에 따른 필터링을 지원하면 바람직할 것 같음(?). 위의 Section 4.3.1.1 Subjects 목록 필터링과 같이 "Authority" 또는 "Organization"에 대한 drop down menu가 필요할 것 같다.

##### 4.7.1.2. User 목록 검색

User 목록 검색은 위의 Section 4.3.1.2 Subjects 목록 검색과 같이 Fig. 4.71 화면 상단 오른쪽 검색창에 검색하고자 하는 Name, Email, Authority, Organization, Customer Type 또는 Last Access Time 값을 입력하면 검색 결과를 볼 수 있다.

![admin_73](img/admin_73.png "User 목록 검색")
<center>Fig. 4.73 User 목록 검색 화면</center>
<br><br>

#### 4.7.2. User 추가/삭제/편집

User에 대한 추가, 삭제 및 편집 기능을 제공한다. User에 대한 이력 관리를 위하여 조직이 삭제되지 않는 한 user를 삭제하지 않는 것을 추천한다.

##### 4.7.2.1. User 추가

User 추가는 Fig. 4.71 User 목록 뷰 화면 상단 "ADD" button을 click하면 Fig. 4.74와 같은 popup window가 나타난다.

![admin_74](img/admin_74.png "User 추가 화면")
<center>Fig. 4.74 User 추가 화면</center>
<br><br>

추가하고자 하는 user의 Name, 국가 코드를 포함한 Phone number, Email, Organization를 긱접 입려하고, drop down menu로 Site, Authority와 Customer Type을 선택하고, Description을 입력한다. 다음 하단 오른쪽 "ADD"를 click하면 새로운 user를 등록할 수 있다. 또한 입력중에 취소하려면 하단 왼쪽 "CANCEL" button을 click하여 취소할 수 있다.

##### 4.4.2.2. User 삭제

이미 등록한 user를 삭제할 수 있다. 이는 이미 위에서 언급하였듯이 소속 Organization이 계속 존재한다면 삭제 하지 않고 해당 사용자를 inactive 상태로 변경하는 것이 바람직하다. 삭제를 위하여는 해당 user 왼쪽 check box를 click하여 check한다 (Fig. 4.75 (a)). 다음 화면 상단 "DEL" button을 click하면 Fig. 4.75 (b) User 삭제 확인 화면이 pup window에 나타난다. 이때 화면 하단의 "DELETE" 또는 "CANCEL" button을 click하며 삭제 또는 취소할 수 있다.

> 사용자의 상태를 "active" 또는 "inactive"로 변경하는 기능이 필요하지 않을까요?

![admin_75_a](img/admin_75_a.png "User 삭제 화면")
<center>Fig. 4.75 (a) User 삭제 화면</center>
<br><br>

![admin_45_b](img/admin_45_b.png "User 삭제 확인 화면")
<center>Fig. 4.75 (b) User 삭제 확인 화면</center>
<br><br>

##### 4.7.2.3. User 정보 편집

User에 대한 정보의 변경 기능을 제공함다. User 맨 오른쪽 연필 모양의 icon을 click하여 해당 user에 대한 정보를 편집할 수 있다. click하면 Fig. 4.76과 같은 화면이 popup window로 나타나며, Name, Phone number, Organization, Authority, Customer Type과 Description을 변경할 수 있다. 데이터를 변경한 다음 화면 하단의 "CANCEL" 또는 "UPDATE" button을 click하면 변경을 취소하거나 수행할 수 있다.

> "Organization"은 변경할 수 없도록 하는 것이 좋을 듯함

![admin_76](img/admin_76.png "User 정보 편집 화면")
<center>Fig. 4.76 User 정보 편집 화면</center>
<br><br>

사용자가 자신의 password를 분실하거나 기억하지 못하는 경우를 대비하여 password를 초기화하는 기능을 하단에 "SEND PASSWORD RESET LINK" button을 제공한다. 이 경우에 관리자는 간단히 button을 click함으로써 사용자가 직접 password를 변경할 수 있도록 한다.

> 사용자 ID (Email)을 기억하지 못할 때를 대비하여야 하지 않을까요? (그것이 Email 이든 다른 ID이던)

### 4.8 Notification 관리

Notification 관리 기능을 제공한다.

> what is notification?

#### 4.8.1. Notification 목록 뷰

 Notification 관리를 위한 화면 즉 Fig. 4.0 Administration 시작 화면 좌측 side bar menu에서 "Notification" button을 click한 다음 보이는 화면으로 Fig. 4.81과 같다.

![admin_81](img/admin_81.png "Notification 목록 뷰 화면")
<center>Fig. 4.81 Notification 목록 뷰 화면</center>
<br><br>

#### 4.8.2. Notification 추가/삭제

Notification에 대한 추가와 삭제 기능을 제공한다.

##### 4.8.2.1. Notification 추가

Notification 추가는 Fig. 4.81 Notification 목록 뷰 화면 상단 "ADD" button을 click하면 Fig. 4.82와 같은 popup window가 나타난다.

![admin_82](img/admin_82.png "Notification 추가 화면")
<center>Fig. 4.82 Notification 추가 화면</center>
<br><br>

추가하고자 하는 notification의 Group Name과 Description을 읿력한 다음 하단 오른쪽 "ADD"를 click하면 새로운 notification을 등록할 수 있다. 또한 입력중에 취소하려면 하단 왼쪽 "CANCEL" button을 click하여 취소할 수 있다.

> Fig. 4.82 화면에서 "CANCEL" 및 close 기능 작동하지 않음.

##### 4.8.2.2. Notification 삭제

이미 등록한 notification을 삭제할 수 있다. 삭제를 위하여는 notification 왼쪽 check box를 click하여 check한다 (Fig. 4.83 (a)). 다음 화면 상단 "DEL" button을 click하면 Fig. 4.83 (b) notification 삭제 확인 화면이 pup window에 나타난다. 이때 화면 하단의 "DELETE" 또는 "CANCEL" button을 click하며 삭제 또는 취소할 수 있다.

> 삭제 선택을 위한 check box 없음

![admin_83_a](img/admin_83_a.png "Notification 삭제 화면")
<center>Fig. 4.83 (a) Notification 삭제 화면</center>
<br><br>

![admin_83_b](img/admin_83_b.png "Notification 삭제 확인 화면")
<center>Fig. 4.83 (b) Notification 삭제 확인 화면</center>
<br><br>


### ?
