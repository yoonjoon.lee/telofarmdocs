## 1. 대시보드

### 1.1. 지도 기반 대시보드

Telofarm로 내부 사용자는 전체 뷰로 부터 시작

로그인 사용자가 슈퍼유저와 뷰어인 경우 로그인 성공 후 Fig 1.1과 같은 대시보드 화면이 나타난다.

![dashboard_01](img/dashboard_01.png "대시보드 화면")
<center>Fig. 1.1 대시보드 화면</center>
<br><br>

> 현장 운영자의 경우 (?) 현장의 지도를 보여 주어야 함.

### 1.2. 통계 뷰

1. 위의 Fig. 1.1 "DASHBOARD"를 click
2. 통계 뷰 페이지로 이동한다.

> (OK)

![dashboard_02](img/dashboard_02.png "통계 뷰")
<center>Fig. 1.2 통계 뷰 화면</center>
<br><br>

#### 1.2.1. 사이트 통계

Section 1.2 실행 후 왼쪽 상단에 Fig. 1.2. 사이트 통계 화면이 나타남.

![dashboard_03](img/dashboard_03.png "사이트 통계")
<center>Fig. 1.3 사이트 통계 화면</center>
<br><br>

#### 1.2.2. 고객 유형 통계

Section 1.2 실행 후 왼쪽 두번째에 Fig. 1.4 고객 유형 통계 화면이 나타남.

![dashboard_04](img/dashboard_04.png "고객 유형 통계")
<center>Fig. 1.4 고객 유형 통계 화면</center>
<br><br>

#### 1.2.3. 모듈 통계

Section 1.2 실행 후 오른쪽 상단에 Fig. 1.5 모듈 통계 화면이 나타남.

![dashboard_05](img/dashboard_05.png "센서/모듈 통계")
<center>Fig. 1.5 모듈 통계 화면</center>
<br><br>

### 1.3. 실시간 데이터 끊김

Section 1.2 실행 후 중앙에 Fig. 1.6 실시간 데이터 끊김 화면이 나타남.

![dashboard_06](img/dashboard_06.png "실시간 데이터 끊김")
<center>Fig. 1.6 실시간 데이터 끊김 화면</center>
<br><br>

> 실시간 데이터가 없어 test 결과를 보일 수 없음

### 1.4. 사이트 카드 뷰

![dashboard_07](img/dashboard_07.png "사이트 카드 뷰")
<center>Fig. 1.7 사이트 카드 뷰 화면</center>
<br><br>

> 사이트 카드 뷰를 위한 설정을 위한 test senario 필요.
> Section 4.1 사이트 관리 (목록 뷰) 참조

#### 1.4.1. 카드 추가

1. Fig. 1.2 통계 뷰 화면의 오른 쪽 상단 설정 버튼을 click한다.
1. Fig. 1.8 사이트 설정 화면의 오른 쪽 메뉴바에서 site 버튼을 click한다.

![dashboard_08](img/dashboard_08.png "사이트 설정")
<center>Fig. 1.8 사이트 설정 화면</center>
<br><br>

3. Fig. 1.9 사이트 카드 설정 화면 상단의 "FAVORITE" 버튼을 click

![dashboard_09](img/dashboard_09.png "사이트 카드 뷰 설정")
<center>Fig. 1.9 사이트 카드 설정 화면</center>
<br><br>

4. Fig. 1.10 사이트 카드 뷰 설정 팝업이 화면에 나타남

![dashboard_10](img/dashboard_10.png "사이트 카드 뷰 설정")
<center>Fig. 1.10 사이트 카드 뷰 설정 화면</center>
<br><br>

5. Fig. 1.11 사이트 카드 뷰 설정 화면의 왼쪽 telefarm checkbox를 click하여 checkbox에 check를 하고 상단의 "+" 버튼을 click하여 오른쪽 "FAVORITE" 컬럼에 나타나도록 한다.

![dashboard_11](img/dashboard_11.png "사이트 카드 뷰 설정")
<center>Fig. 1.11 사이트 카드 뷰 설정 화면</center>
<br><br>

6. 하단 오른쪽의 "UPDATE" 버튼을 click하면 팝업 윈도우가 닫힘.
7. 이후 상단 오른쪽의 "TELOFARM"을 click하고, 바로 아래의 "DASHBOARD"를 click하면
8. Fig. 1.2 통계 뷰 화면과 같은 화면임을 확임

#### 1.4.2. 카드 추가 취소

1. Fig. 1.2 통계 뷰 화면의 오른 쪽 상단 설정 버튼을 click한다.
2. Fig. 1.8 사이트 설정 화면의 오른 쪽 메뉴바에서 site 버튼을 click한다.

![dashboard_08](img/dashboard_08.png "사이트 설정")
<center>Fig. 1.8 사이트 설정 화면</center>
<br><br>

3. Fig. 1.9 사이트 카드 설정 화면 상단의 "FAVORITE" 버튼을 click

![dashboard_09](img/dashboard_09.png "사이트 카드 뷰 설정")
<center>Fig. 1.9 사이트 카드 설정 화면</center>
<br><br>

4. Fig. 1.10 사이트 카드 뷰 설정 팝업이 화면에 나타남

![dashboard_10](img/dashboard_10.png "사이트 카드 뷰 설정")
<center>Fig. 1.10 사이트 카드 뷰 설정 화면</center>
<br><br>

5. Fig. 1.12 사이트 카드 뷰 설정 화면의 왼쪽 sample_site checkbox를 click하여 checkbox에 check를 하고 상단의 "+" 버튼을 click하여 오른쪽 "FAVORITE" 컬럼에 나타나도록 한다.

![dashboard_12](img/dashboard_12.png "사이트 카드 뷰 설정")
<center>Fig. 1.12 사이트 카드 뷰 설정 화면</center>
<br><br>

6. 하단 오른쪽의 "CANCEL" 버튼을 click하면 팝업 윈도우가 닫힘.
7. 이후 상단 오른쪽의 "TELOFARM"을 click하고, 바로 아래의 "DASHBOARD"를 click하면
8. Fig. 1.2 통계 뷰와 같은 화면임을 확임

#### 1.4.3. 카드 삭제

1. Fig. 1.2 통계 뷰 화면의 오른 쪽 상단 설정 버튼을 click한다.
2. Fig. 1.8 사이트 설정 화면의 오른 쪽 메뉴바에서 site 버튼을 click한다.

![dashboard_08](img/dashboard_08.png "사이트 설정")
<center>Fig. 1.8 사이트 설정 화면</center>
<br><br>

3. Fig. 1.9 사이트 카드 설정 화면 상단의 "FAVORITE" 버튼을 click

![dashboard_09](img/dashboard_09.png "사이트 카드 뷰 설정")
<center>Fig. 1.9 사이트 카드 설정 화면</center>
<br><br>

4. Fig. 1.10 사이트 카드 뷰 팝업이 화면에 나타남

![dashboard_10](img/dashboard_10.png "사이트 카드 뷰 설정")
<center>Fig. 1.10 사이트 카드 뷰 설정 화면</center>
<br><br>

5. 취소 하고자하는 사이트의 checkbox를 uncheck하고 상단의 "+" 버튼을 click하여 오른쪽 "FAVORITE" 컬럼에서 Fig. 1.10 사이트 카드 뷰 화면과 같이 삭제되었음을 확인한다.
6. 하단 오른쪽의 "UPDATE" 버튼을 click하면 팝업 윈도우가 닫힘.
7. 이후 상단 오른쪽의 "TELOFARM"을 click하고, 바로 아래의 "DASHBOARD"를 click하면
8. 삭제된 사이트 카드가 사라졌음을 확인

#### 1.4.4. 카드 삭제 취소

1. Fig. 1.2 통계 뷰 화면의 오른 쪽 상단 설정 버튼을 click한다.
2. Fig. 1.8 사이트 설정 화면의 오른 쪽 메뉴바에서 site 버튼을 click한다.

![dashboard_08](img/dashboard_08.png "사이트 설정")
<center>Fig. 1.8 사이트 설정 화면</center>
<br><br>

3. Fig. 1.9 사이트 카드 설정 화면 상단의 "FAVORITE" 버튼을 click

![dashboard_09](img/dashboard_09.png "사이트 카드 뷰 설정")
<center>Fig. 1.9 사이트 카드 설정 화면</center>
<br><br>

4. Fig. 1.10 사이트 카드 뷰 설정 팝업이 화면에 나타남

![dashboard_10](img/dashboard_10.png "사이트 카드 뷰 설정")
<center>Fig. 1.10 사이트 카드 뷰 설정 화면</center>
<br><br>

5. 취소 하고자하는 사이트의 checkbox를 uncheck하고 상단의 "+" 버튼을 click하여 오른쪽 "FAVORITE" 컬럼에서 Fig. 1.10 사이트 카드 뷰 설정 화면과 같이 삭제되었음을 확인한다.
6. 하단 오른쪽의 "CANCEL" 버튼을 click하면 팝업 윈도우가 닫힘.
7. 이후 상단 오른쪽의 "TELOFARM"을 click하고, 바로 아래의 "DASHBOARD"를 click하면
8. 사이트 카드가 삭제되지 않았음을 확인

### 1.5. 알림 표시

> 2021-04-16 현재 알람 목록만 보임. under development

#### 1.5.1. 알림 목록

#### 1.5.2. 알림 카드(토스트) 표시

#### 1.5.3. 알림 대상

##### 1.5.3.1. 1시간 이상 데이터 끊김 지속 모듈

##### 1.5.3.2. 시스템 이상

##### 1.5.3.3. 질병 등 이상 상태 감지 subject

##### 1.5.3.4. 추가 모듈 등록 요청
