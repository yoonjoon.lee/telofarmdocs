## 3. 장치(Devices) 메뉴

장치 관리 서비스는 고객 사이트를 구성하기 위하여 사이트에 장치 설치 형상을 구성, 변경할 수 있는 서비스를 제공한다. 이는 로그인 후 나타나는 Fig. 1.1 대시보드 화면 상단에 "DEVICES" 버튼을 click하여 시작할 수 있다. 따라서 모든 senario는 로그인 성공을 포함하여야 한다. 이 메뉴는 "슈퍼유저"와 "현장관리자" role을 수행하는 사용자에게만 제공된다. (2021-05-10 현재 "수퍼유저" 대상으로만 작성함)

> 이 메뉴에서 하는 기능에 대하여 명확한 정의가 필요. 즉 telofam내의 모든 장치에 관한 것임
> (3.1.3 포함).
>

### 3.1. 장치(모듈) 목록 화면

Fig. 1.1 대시보드 화면 상단에서 "DEVICES" 버튼을 click한 후 사용자에게 보여주는 Fig. 3.1 장치 목록 화면이다.

![dev_mgt_01](img/dev_mgt_01.png "장치 목록 화면")
<center>Fig. 3.1 장치 목록 화면</center>
<br><br>

아래 기술된 모든 senario는 위의 Fig. 3.1 화면에서 시작한다. 이때 default로 "Devices/ALL"로 모든 device에 대한 정보를 보여 준다 (수퍼유저, 현장관리자 사용자 경우). device 목록은 최초 로딩시 "Type (device type)" ASC 순으로 정렬되도록 설정되어 있다. 이를 변경하려면 각 행 이름에 마우스를 올려 놓으면 화살표가 나타나고 up arrow 또는 down arrow 선택에 따라 ASC 또는 DESC로 목록을 정렬할 수 있다.

#### 3.1.1. 장치 조회 (관리 이력 포함)

> 화면 오른쪽 상단의 검색창의 기능이 애매함(?)

Device list에서 행(단  Site와 Product Name 제외)을 click하면, 장치에 대한 정보를 popup 화면으로 보여준다. Device Type이 "Gateway"와 "Sensor"인 경우 "DIV INFO"와 "HISTORY"를 (Fig. 3.2), "Module"인 경우 "DIV INFO", "HISTORY", "RAWDATA"와 "CALDATA"로 나누어 보여준다 (Fig. 3.3). "DIV INFO"에는 device의 ID, Type, Product Name, 연결 벙보 (Gateway, Channel), 입고일(Warehousing date), 배송일(Shipment date), 등록일(Regiser date)과 최종변경일(Last Modified)에 대한 정보를 보여 준다.

"HISTORY"에서는 관리이력을 보여준다. "Name"은 device 관리 작업을 수행한 사용자를 나타낸다. "Code"는 작업 코드, "DESC"는 작업에 대한 설명이며, "Date"는 작업 수행 일시이다.

"Product Name"을 clcik 하면 Product 에 대한 정보를 출력하며, "Site"를 click하면 해당 사이트의 지도를 보여준다. 화면 오른쪽 상단의 검색창에서 조건을 입력하여 filter 기능을 추가로 수행할 수 있다.

> "Name"을 "User"로 변경하는 것을 추천

![dev_mgt_02](img/dev_mgt_02.png "Gateway와 Sensor type 장치 정보 화면")
<center>Fig. 3.2 Gateway와 Sensor type 장치 정보 화면</center>
<br><br>

![dev_mgt_03](img/dev_mgt_03.png "Module type 장치 정보 화면")
<center>Fig. 3.3 Module type 장치 정보 화면</center>
<br><br>

Device list에서 Product Name 컬럼의 한 값을 click하면 Product Information popup 창이 나타난다 (Fig. 3.3-1 (a)). 이 창은 "DIV INFO"(Fig. 3.3-1 (a))와 "CHILDABLE"(Fig. 3.3-1 (b))로 구성되어 있다.

> 이때 "DIV INFO" 화면에서 ID, Dispaly Name, Type ID, Algorithm ID, Description, Created 는 무엇을 의미 하는가?

> Product와 Device 간의 관계가 명확하지 않음. (용어 사용에 표준화가 필요함)
> Product Information의 경우 ID가 sample-gateway이고 장치 ID인 것은 바람직하지 않은 듯.

![dev_mgt_03_1](img/dev_mgt_03_1.png "Product Information 창에서 DIV INFO 화면")
<center>Fig. 3.3-1 (a) Product Information 창에서 DIV INFO 화면</center>
<br><br>

![dev_mgt_03_2](img/dev_mgt_03_2.png "Product Information 창에서 CHILDABLE 화면")
<center>Fig. 3.3-1 (b) Product Information 창에서 CHILDABLE 화면</center>
<br><br>

> Fig. 3.3-1 (a) 과 Fig. 3.3-1 (b)에서 볼 수 있는 "HISTORY"는 동일한 정보이라면 둘 중 하나에서만 보여주는 것이 바람직하지 않을까요?

"CHILDABLE"에서는 이 product에 연결 가능한 product를 나타내고 있다. Gateway type Product인 경우 연결 가능한 Module Type Product를, Module Type Product인 경우 Sensor Type Product를 보여준다.

##### 3.1.1.1. 신호 확인

Fig. 3.1 장치 목록 화면에서 컬럼명 "Status"를 나타낸다. 이는 각 device type에 따라 Section 3.1.1.3., Section 3.1.1.4.와 Section 3.1.1.5 에서도 가능하다.

> (2021-04-30 현재) 모두 "OK" 상태만 나타남.

> 이는 실제 상황을 모사할 수 있어야 test가 가능할 것으로 보임.

##### 3.1.1.2. 장치 관리 이력 추가

> (2021-05-10 현재) device 추가, 변경에 대한 이력이 없어 관리되고 있지 않음 (?). (To be implemented)

##### 3.1.1.3. Gateway 장치 조회

Fig. 3.1 장치 목록 화면에서 왼쪽 side bar 메뉴에서 "Geteway" 우측 down arrow를 click하면 설치죈 모든 Product Type이 "Gateway"인 장치(Gateway Product Name)목록을 side bar에 보여 준다 (Fig. 3.4 Gateway Product Name 목록 화면). 이때 원하는 펼쳐진 Gateway Product Name 중 하나를 click하면 해당 Gateway Product가 install된 product (장치) 목록을 보여준다 (Fig. 3.5 Gateway Product Name 장치 목록 화면).

이는 기본적으로 모든 장치 조회 (Fig. 3.1 장치 목록 화면)에서 Product Type이 "Gateway"인 장치를 보여주는 것과 동일한 것이다 (Fig. 3.5 Gateway Product Name 장치 목록 화면).

![dev_mgt_04](img/dev_mgt_04.png "Gateway Product Name 조회 화면")
<center>Fig. 3.4 Gateway Product Name 목록 화면</center>
<br><br>

![dev_mgt_05](img/dev_mgt_05.png "Gateway Product Name 장치 목록 화면")
<center>Fig. 3.5 Gateway Product Name 장치 목록 화면</center>
<br><br>

##### 3.1.1.4. Module 장치 조회

Fig. 3.1 장치 목록 화면에서 왼쪽 side bar 메뉴에서 "Module" 우측 down arrow를 click하면 설치죈 모든 Product Type이 "Module"인 장치(Module Product Name)목록을 side bar에 보여 준다 (Fig. 3.6 Module Product Name 목록 화면). 이때 원하는 펼쳐진 Module Product Name 중 하나를 click하면 해당 Module Product가 install된 product(장치) 목록을 보여준다 (Fig. 3.7 Module Product Name 장치 목록 화면).

![dev_mgt_06](img/dev_mgt_06.png "Module Product Name 조회 화면")
<center>Fig. 3.6 Module Product Name 목록 화면</center>
<br><br>

![dev_mgt_07](img/dev_mgt_07.png "Module Product Name 장치 목록 화면")
<center>Fig. 3.7 Module Product Name 장치 목록 화면</center>
<br><br>

##### 3.1.1.5. Sensor 장치 조회

Fig. 3.1 장치 목록 화면에서 왼쪽 side bar 메뉴에서 "Sensor" 우측 down arrow를 click하면 설치죈 모든 Product Type이 "Sensor"인 장치(Sensor Product Name)목록을 side bar에 보여 준다 (Fig. 3.8 Sensor Product Name 목록 화면). 이때 원하는 펼쳐진 Sensor Product Name 중 하나를 click하면 해당 Sensor Product가 install된 product(장치) 목록을 보여준다 (Fig. 3.9 Sensor Product Name 장치 목록 화면).

![dev_mgt_08](img/dev_mgt_08.png "Sensor Product Name 조회 화면")
<center>Fig. 3.8 Sensor Product Name 목록 화면</center>
<br><br>

![dev_mgt_09](img/dev_mgt_09.png "Sensor Product Name 장치 목록 화면")
<center>Fig. 3.9 Sensor Product Name 장치 목록 화면</center>
<br><br>

#### 3.1.2. 장치 추가/삭제

장치 추가에 앞서 장치의 product를 등록하여야 한다 (Section 4.4 참조). 현재 장치에는 Gateway Type인 Product Name으로 Gateway, Module Type인 Product Name으로 PLS100, PLS100 SAP_flow과 PLS100 Soil, Sensor Type인 Product Name으로 SAP-flow sensor, Soil sensor, Humidity sensor와 Temperature sensor만 등록되어 있음. 따라서 Product Type에 속하는 Product Name을 등록할 수 있어야 함. 또한 각 device는 device ID를 갖고 있으며 특정한 한 Product Name에 속하여야 함.

##### 3.1.2.1 Device(장치) 추가

모든 Device 추가는 Fig. 3.1 장치 목록 화면에서 화면 상단의 "ADD" 버튼을 click하여 수행할 수 있다 (Fig. 3.11 Device 추가 화면).

![dev_mgt_11](img/dev_mgt_11.png "Device 추가 화면")
<center>Fig. 3.11 Device 추가 화면</center>
<br><br>

1. Device Type을 선택한다.
2. 선택된 device type의 Product Name을 선택한다.
3. 고객의 Site_id를 선택한다.
4. device id를 입력한다.
5. 선택 사항인 Serial Number, Firmware Version, Location(Address?), Location(Latitude, Longitude)를 입력한다.
6. 하단의 "ADD" 버튼을 click하여 추가허거나 "CANCEL" 버튼을 click하여 취소한다.

> device-id(device의 unique key)는 시스템에서 제공하거나 device의 serial number로 하는 것이 어떨까요?
> 위의 Location이 2개 있으므로 구별할 필요가 있지 않을까요?

Device Type은 Section 3.1.1.3의 Fig. 3.5 Gateway Product Name 장치 목록 화면에서, Module Type은 Section 3.1.1.4의 Fig. 3.7 Module Product Name 장치 목록 화면에서, Device Type은 Section 3.1.1.5의 Fig. 3.9 Sensor Product Name 장치 목록 화면에서도 위와 같은 방법으로 Device를 추가할 수 있다.

> 이때, 이미 Gateway Type과 Product Name이 결정되었으므로 이를 화면에 표시하여 주는 것이 바람직 하지 않을까요? 이는 Module Type과 Sensor Type Device 추가시에도 동일하게 처라하면 어떨까요?

##### 3.1.2.1 Device(장치) 삭제

모든 Device를 Fig. 3.1 장치 목록 화면에서 장치 row의 첫번째 column의 box를 click한 다음 화면 상단의 "DEL" 버튼을 click하여 삭제할 수 있다 (Fig. 3.12). 이때 한번에 오직 **한** device만 삭제할 수 있다.

![dev_mgt_12](img/dev_mgt_12.png "Device 삭제 화면")
<center>Fig. 3.12 Device 삭제 화면</center>
<br><br>

Device를 Device Type은 Section 3.1.1.3의 Fig. 3.5 화면에서, Module Type은 Section 3.1.1.4의 Fig. 3.7 화면에서, Device Type은 Section 3.1.1.5의 Fig. 3.9 화면에서도 위와 같은 방법으로 삭제할 수 있다.

#### 3.1.3. 장치 편집 (연결 관리 포함)

장치 편집은 Section 3.1 모든 장치 조회 화면 Fig. 3.1과 Device Type에 따른 조회 Section 3.1.1.3, 3.1.1.4와 3.1.1.5의 화면 Fig. 3.5, Fig. 3.7과 Fig. 3.9에서 수행할 수 있다.

장치 조회 화면 Fig. 3.1에서 편집하고자 하는 device 행의 마지막 컬럼인 "EDIT" 컬럼의 연필 표시를 click하면 Fig. 3.20 (a)과 같은 popup 창이 나타난다. 이는 "INFO"(Fig. 3.10 (a))와 "CHILD"(Fig. 3.20 (b))로 구성되어 있다. INFO화면에서는 device의 정보를 편집할 수 있으며, CHILD화면에서는 연결정보를 편집랄 수 있다.

![dev_mgt_20_1](img/dev_mgt_20_1.png "Gateway Type Device 정보 편집 화면")
<center>Fig. 3.20 (a) Gateway Type Device 정보 편집 화면</center>
<br><br>

> 값이 있으면 음영으로 처리했음. 없으면 column name

연결 정보 편집을 위하여는 Fig. 3.20 (a) 화면에서 "CHILD"를 클릭하면 Fig. 3.20 (b) 화면이 나타난다. 이때 연결된 device를 삭제하려면 해당 device 행의 왼ㅈ쪽 첫번째 컬럼의 box를 check하고 상단의 "DEL"을 click한다.

![dev_mgt_20_2](img/dev_mgt_20_2.png "Gateway Type Device 연결 편집 화면")
<center>Fig. 3.20 (b) Gateway Type Device 연결 편집 화면</center>
<br><br>

> Gateway Type device에 Module Type device만 연결할 수 있다면 "Type"이 필요 없을 것이며, 그렇하지 않더라도 "Type" 옆의 화살표가 필요할까요?

새로운 device를 연결하고자 할때는 "ADD"를 click하면, 현재 연결하고자 하는 device의 check box를 click하여 check하고 오른쪽 하단의 "ADD"를 click하여 연결을 추가한다.

![dev_mgt_21](img/dev_mgt_21.png "Gateway Type Device 연결 추가 화면")
<center>Fig. 3.21 Gateway Type Device 연결 추가 화면</center>
<br><br>

> 연결 추가시 가능한 device 목록만 보여주는 것이 바람직할 것 같음
> 또한, 연결 추가는 하위 device에서 하는 것도 고려하여 보시기 바랍니다.

##### 3.1.3.1 Gateway Type Device 편집

Section 3.1.3에서 이미 언급하였듯이 모든 device 목록에서 Gateway Type Device 편집에 대하여 기술하였으므로 이를 참조하시오.

##### 3.1.3.2 Module Type Device 편집

Section 3.1.3.1과 동일 하나 연결 접보 편집 화면에서 "ADD"를 click하면 보여주는 화면(Fig. 3.22 Module Type Device 연결 추가 화면)에서 연결 가능한 device만을 보여주어야 함.

![dev_mgt_22](img/dev_mgt_22.png "Module Type Device 연결 추가 화면")
<center>Fig. 3.22 Module Type Device 연결 추가 화면</center>
<br><br>

> Module Type device에서 Module Type device 연결이 가능한가? 가능하지 않다면 이를 리스트에서 제거하는 것이 바람직함. (childable에 정의되어 있지 않음)

##### 3.1.3.3 Sensor Type Device 편집

Section 3.1.3.1과 동일 하나 Sensor Type device에 다른 device를 연결할 수 없다면, Fig. 3.20 (a) Gateway Type Device 정보 편집 화면에서 "CHILD"를 삭제하는 것이 바람직할 것으로 사려됨.

#### 3.1.4. 장치 입출고 관리
**To be extended**
