### 4.7. Users(사용자) 관리

사용자 등록

> 2020-04-08 현재 사용자는 Master, SiteManager, FieldManager와 Unapproved(?) 4종류로 나누어져 있음.

> 2020-04-09 현재  Master, SiteManager와 FieldManager(?)의 Authority 사용자만 사용자 등록 권한을 갖고 있으나 사용자 등록 미구현으로 Master 사용자의 사용자 등록 test senario만을 작성. 이후 권한에 따른 등록 권한이 구현된 다음 SiteManager와 FieldManager(?)의 Authority 사용자에 대한 test senario 작성 필요.

Master 사용자 로그인 후, 오른쪽 상단 설정 버튼을 클릭하고, 왼쪽 메뉴 바에서 "Users"를 클릭하여 사용자 관리 화면으로 이동하여 시작한다.

> 2020-04-08 현재, 사용자 등록은 가능하나 password를 setting할 수 없어 다른 사용자에 대한 test는 수행하여야 함


![user_mgt_01](img/user_mgt_01.png "사용자 관리 화면")
<center>Fig. 1.1 사용자 관리 화면</center>
<br><br>

![user_regis_01](img/user_regis_01.png "사용자 등록 화면")
Fig. 1.2 사용자 등록 화면
<center>Fig. 1.2 사용자 관리 화면</center>
<br><br>

> 2020-04-08 현재
>  - 사용자 등록시 password 등록 절차 없음.
>  - "Organization" 보다는 "Site(?)" 또는 ?
>

#### 1.2.1. Master 사용자 등록

##### 1.2.1.1. Master 사용자 등록 완료

> 2020-04-09 현재 Master 사용자의 등록은 가능하나 password를 Setting할 수 없어 등록 후, 로그린 등의 후속 tesst를 할 수 없음.

1. 사용자 관리 화면(Fig. 1.1)에서 Name에 사용자 이름 입력 "YoonJoon Lee"

> "Name "대신 "User ID"가 바람직 할 둣

1. 전화번호 입력 country code: "82" Phone Number:"010-4411-3523"
1. Email 입력 E-Mail: "yoonjoon.lee@suredatalab.com"
1. Organization 입력: "SureDataLab"
1. Authority 선택: "Master"
1. Desciption: "For Test" 입력
1. "ADD" 버튼 click
1. 사용자 추가 팝업을 close한 다음
1. 사용자 관리화면에서 입력한 사용자의 등록을 확인함. ("User Add Success" 메시지 확인)

##### 1.2.1.2. Master 사용자 등록시 필수 항목 (Name) 미입력 오류

1. 위의 Fig. 1.2 에서 Name에 사용자 이름 미입력
1. 오류 메시지 ("Name is required") 확인 이후
1. Name에 사용자 이름 입력 "YoonJoon Lee"
1. 전화번호 입력 country code: "82" Phone Number:"010-4411-3523"
1. Email 입력 E-Mail: "yoonjoon.lee@suredatalab.com"
1. Organization 입력: "SureDataLab"
1. Authority 선택: "Master"
1. Desciption: "For Test" 입력
1. "ADD" 버튼 click
1. 사용자 추가 메시지 확인
1. 사용자 추가 팝업을 close한 다음
1. 사용자 관리화면에서 입력한 사용자의 등록을 확인함.

##### 1.2.1.3. Master 사용자 등록시 필수 항목 (E-mail) 미입력 오류

1. 위의 Fig. 1.2 에서 Name에 사용자 이름 입력 "YoonJoon Lee"
1. 전화번호 입력 country code: "82" Phone Number:"010-4411-3523"
1. Email 미입력
1. 오류 메시지 ("E-Mail is required") 확인 이후
1. Email 입력 E-Mail: "yoonjoon.lee@suredatalab.com"
1. Organization 입력: "SureDataLab"
1. Authority 선택: "Master"
1. Desciption: "For Test" 입력
1. "ADD" 버튼 click
1. 사용자 추가 메시지 확인
1. 사용자 추가 팝업을 close한 다음
1. 사용자 관리화면에서 입력한 사용자의 등록을 확인함.

##### 1.2.1.4. Master 사용자 등록시 필수 항목 (Organization) 미입력 오류

1. 위의 Fig. 1.2 에서 Name에 사용자 이름 입력 "YoonJoon Lee"
1. 전화번호 입력 country code: "82" Phone Number:"010-4411-3523"
1. Email 입력 E-Mail: "yoonjoon.lee@suredatalab.com"
1. Organization 미입력
1. 오류 메시지 ("Organization is required") 확인 이후
1. Organization 입력: "SureDataLab"
1. Authority 선택: "Master"
1. Desciption: "For Test" 입력
1. "ADD" 버튼 click
1. 사용자 추가 메시지 확인
1. 사용자 추가 팝업을 close한 다음
1. 사용자 관리화면에서 입력한 사용자의 등록을 확인함.

##### 1.2.1.5. Master 사용자 등록시 필수 항목 (Authority) 미선택 오류

1. 위의 Fig. 1.2 에서 Name에 사용자 이름 입력 "YoonJoon Lee"
1. 전화번호 입력 country code: "82" Phone Number:"010-4411-3523"
1. Email 입력 E-Mail: "yoonjoon.lee@suredatalab.com"
1. Organization 입력: "SureDataLab"
1. Authority 미선택: "Master"
1. 오류 메시지 ("Authority is required") 확인 이후
1. Authority 선택: "Master"
1. Desciption: "For Test" 입력
1. "ADD" 버튼 click
1. 사용자 추가 메시지 확인
1. 사용자 추가 팝업을 close한 다음
1. 사용자 관리화면에서 입력한 사용자의 등록을 확인함.

##### 1.2.1.6. Master 사용자 중복 등록 오류

> 이미 "YoonJoon Lee"라는 이름의 Master Authority (?)를 갖는 사용자 존재

1. 위의 Fig. 1.2 에서 Name에 사용자 이름 입력 "YoonJoon Lee"
1. 전화번호 입력 country code: "82" Phone Number:"010-4411-3523"
1. Email 입력 E-Mail: "yoonjoon.lee@suredatalab.com"
1. Organization 입력: "SureDataLab"
1. Authority 입력 불가능 "No data available" 메시지 출력 (**대신 다른 메시지 출력 필요함 "빨간색"**)
1. "CANCEL" 버튼 click
1. 사용자 추가 팝업이 사라짐.

#### 1.2.2. SiteManager 사용자 등록

> 2020-04-09 현재 SiteManager 사용자의 등록은 가능하나 password를 Setting할 수 없어 등록 후, 로그린 등의 후속 tesst를 할 수 없음.

##### 1.2.2.1. SiteManager 사용자 등록

1. 위의 Fig. 1.2 에서 Name에 사용자 이름 입력 "JeongHoon Lee"
1. 전화번호 입력 country code: "82" Phone Number:"010-3272-1250"
1. Email 입력 E-Mail: "jh.lee@telofarm.com"
1. Organization 입력: "Telofarm"
1. Authority 선택: "SiteManager"
1. Desciption: "For Test" 입력
1. "ADD" 버튼 click
1. 사용자 추가 팝업을 close한 다음
1. 사용자 관리화면에서 입력한 사용자의 등록을 확인함.

##### 1.2.2.2. SiteManager 사용자 등록시 필수 항목 (Name) 미입력 오류

1. 위의 Fig. 1.2 에서 Name에 사용자 이름 미입력
1. 오류 메시지 ("Name is required") 확인 이후
1. Name에 사용자 이름 입력 "JeongHoon Lee"
1. 전화번호 입력 country code: "82" Phone Number:"010-3272-1250"
1. Email 입력 E-Mail: "jh.lee@telofarm.com"
1. Organization 입력: "Telofarm"
1. Authority 선택: "SiteManager"
1. Desciption: "For Test" 입력
1. "ADD" 버튼 click
1. 사용자 추가 메시지 확인
1. 사용자 추가 팝업을 close한 다음
1. 사용자 관리화면에서 입력한 사용자의 등록을 확인함.

##### 1.2.2.3. SiteManager 사용자 등록시 필수 항목 (E-mail) 미입력 오류

1. 위의 Fig. 1.2 에서 Name에 사용자 이름 입력 "JeongHoon Lee"
1. 전화번호 입력 country code: "82" Phone Number:"010-3272-1250"
1. Email 미입력
1. 오류 메시지 ("E-Mail is required") 확인 이후
1. Email 입력 E-Mail: "jh.lee@telofarm.com"
1. Organization 입력: "Telofarm"
1. Authority 선택: "SiteManager"
1. Desciption: "For Test" 입력
1. "ADD" 버튼 click
1. 사용자 추가 메시지 확인
1. 사용자 추가 팝업을 close한 다음
1. 사용자 관리화면에서 입력한 사용자의 등록을 확인함.

##### 1.2.2.4. SiteManager 사용자 등록시 필수 항목 (Organization) 미입력 오류

1. 위의 Fig. 1.2 에서 Name에 사용자 이름 입력 "JeongHoon Lee"
1. 전화번호 입력 country code: "82" Phone Number:"010-3272-1250"
1. Email 입력 E-Mail: "jh.lee@telofarm.com"
1. Organization 미입력
1. 오류 메시지 ("Organization is required") 확인 이후
1. Organization 입력: "Telofarm"
1. Authority 선택: "SiteManager"
1. Desciption: "For Test" 입력
1. "ADD" 버튼 click
1. 사용자 추가 메시지 확인
1. 사용자 추가 팝업을 close한 다음
1. 사용자 관리화면에서 입력한 사용자의 등록을 확인함.

##### 1.2.2.5. SiteManager 사용자 등록시 필수 항목 (Authority) 미선택 오류

1. 위의 Fig. 1.2 에서 Name에 사용자 이름 입력 "JeongHoon Lee"
1. 전화번호 입력 country code: "82" Phone Number:"010-3272-1250"
1. Email 입력 E-Mail: "jh.lee@telofarm.com"
1. Organization 입력: "Telofarm"
1. Authority 미선택: "Master"
1. 오류 메시지 ("Authority is required") 확인 이후
1. Authority 선택: "SiteManager"
1. Desciption: "For Test" 입력
1. "ADD" 버튼 click
1. 사용자 추가 메시지 확인
1. 사용자 추가 팝업을 close한 다음
1. 사용자 관리화면에서 입력한 사용자의 등록을 확인함.

##### 1.2.2.6. SiteManager 사용자 중복 등록 오류

> 이미 "YoonJoon Lee"라는 이름의 Master Authority (?)를 갖는 사용자 존재

1. 위의 Fig. 1.2 에서 Name에 사용자 이름 입력 "JeongHoon Lee"
1. 전화번호 입력 country code: "82" Phone Number:"010-3272-1250"
1. Email 입력 E-Mail: "jh.lee@telofarm.com"
1. Organization 입력: "Telofarm"
1. Authority 입력 불가능 "No data available" 메시지 출력 (**대신 다른 메시지 출력 필요함 "빨간색"**)
1. "CANCEL" 버튼 click
1. 사용자 추가 팝업이 사라짐.

#### 1.2.3. FieldManager 사용자 등록

> 2020-04-09 현재 SiteManager 사용자의 등록은 가능하나 password를 Setting할 수 없어 등록 후, 로그린 등의 후속 tesst를 할 수 없음.

#### 1.2.4. Unapproved 사용자 등록

> 2020-04-09 현재 SiteManager 사용자의 등록은 가능하나 password를 Setting할 수 없어 등록 후, 로그린 등의 후속 tesst를 할 수 없음.

#### 1.2.5. 사용자 등록 취소

> 모든 사용자 등록 중 취소는 동일함. 위의 Fig. 1.2 에서 입력 중 언제 든지 "CANCEL" 버튼을 click하여 수행함.

1. 위의 Fig. 1.2 에서 이름 입력 "YoonJoon Lee"
1. 전화번호 입력 country code: "82" Phone Number:"010-4411-3523"
1. Email 입력 E-Mail: "yoonjoon.lee@suredatalab.com"
1. Organization 입력: "SureDataLab"
1. Authority 선택: "Master"
1. Desciption: "For Test" 입력
1. "CANCEL" 버튼 click
1. 사용자 추가 popup window 사라짐.
1. 사용자 관리 화면에서 입력한 사용자의 미등록을 확인함.

### 1.3. 사용자 삭제

사용자 관리 화면(Fig. 1.1)에서 모든 사용자를 삭제할 수 있다. 다만 사용자의 Authority (Role)과 Organization에 따라 display되는 사용자가 리스트가 다르고 리스트의 사용자를 삭제할 수 있다. (**20201-04-09 현재 'Master' Authority만 구현되어 있는 듯**)

### 1.3.1. 사용자 삭제

1. 사용자 관리 화면(Fig. 1.1)에 보이는 사용자 리스트 중 삭제할 사용자 이름 앞의 check box clicek
2. 상단 "DEL" 버튼 click
3. 삭제 확인 popup windowd 상 "DELETE" 버튼 click
4. 삭제 확인 popup window가 사라진 후 사용자 삭제 학인

> 무엇인가? 좀 더 확인이 필요하지 않을까?

### 1.3.2. 사용자 삭제 취소

1. 사용자 관리 화면(Fig. 1.1)에 보이는 사용자 리스트 중 삭제할 사용자 이름 앞의 check box click
2. 상단 "DEL" 버튼 click
3. 삭제 확인 popup window 상 "CANCEL" 버튼 click
4. 삭제 확인 popup window가 사라진 후 사용자 리스트 학인
